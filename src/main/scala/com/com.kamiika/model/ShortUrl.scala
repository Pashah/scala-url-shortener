package com.kamiika.model

case class ShortUrl(
  val shortUrl:  String,
  val originalUrl:   String
                     )