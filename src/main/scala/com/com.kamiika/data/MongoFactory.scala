package com.kamiika.data

import com.mongodb.casbah.{MongoClient, MongoCollection, MongoConnection}

/*
MongoDB factory. Contains MongoDB settings.
TODO: Add a properties file which contains MongoDB urls and ports
 */
object MongoFactory {

  private val PORT = 27017
  private val DATABASE = "customerDb"
  private val COLLECTION = "shortUrl"

  val mongoClient =  MongoClient()
  val db = mongoClient(DATABASE)
  val collection = db(COLLECTION)
  def getCollection: MongoCollection = collection

}