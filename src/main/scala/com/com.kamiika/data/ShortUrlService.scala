package com.kamiika.data

import com.kamiika.model.ShortUrl
import com.mongodb.casbah.Imports._

import scala.util.Random

/*
This class contains basic service to short urls and database access
methods
 */
class ShortUrlService {


  val collection = MongoFactory.getCollection

  def createShortUrl(url: String, shortName: Option[String]): String = {
    val shortUrl = shortName.getOrElse(generateRandomUrl())
    if(isShortUrlAlreadyCreated(shortUrl))
      createShortUrl(url, None)
    val shortUrlService = new ShortUrlService
    val shortUrlObj = new ShortUrl(shortUrl, url)
    shortUrlService.saveShortUrl(shortUrlObj)
    shortUrlObj.shortUrl
  }

  def shortUrlNameIsFree(shortName: Option[String]): Boolean = {
    if(shortName.isEmpty)
      true
    else
      !isShortUrlAlreadyCreated(shortName.get)
  }

  private def generateRandomUrl() = {
    Random.alphanumeric.take(6).mkString
  }

  private def isShortUrlAlreadyCreated(shortUrl: String) = {
    getShortUrl(shortUrl) != null
  }

  def saveShortUrl(shortUrl: ShortUrl) = {
    val shortUrlObj = buildMongoDbObject(shortUrl)
    val result = collection.save(shortUrlObj)
    result
  }

  def getShortUrl(shortUrl: String): ShortUrl = {
    val q = MongoDBObject("shortUrl" -> shortUrl)
    val result = collection findOne q
    if(result.isEmpty)
      return null
    val shortUrlResult = result.get
    ShortUrl(originalUrl = shortUrlResult.as[String]("originalUrl"),
      shortUrl = shortUrlResult.as[String]("shortUrl"))
  }

  //Convert ShortUrlService object into a BSON format that MongoDb can store.
  private def buildMongoDbObject(shortUrl: ShortUrl): MongoDBObject = {
    val builder = MongoDBObject.newBuilder
    builder += "originalUrl" -> shortUrl.originalUrl
    builder += "shortUrl" -> shortUrl.shortUrl
    builder.result
  }

}