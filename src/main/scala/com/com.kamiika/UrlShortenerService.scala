package com.kamiika

import akka.actor.Actor
import com.kamiika.data.{ShortUrlService}
import spray.routing._
import spray.http._
import MediaTypes._
import org.apache.commons.validator.routines.UrlValidator

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class UrlShortenerServiceActor extends Actor with UrlShortenerService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(route)
}


// this trait defines our service behavior independently from the service actor
trait UrlShortenerService extends HttpService {

  val shorten =
    path("shorten") {
      post {
        parameter('url, 'shortName.?) { (url, shortName) =>
          respondWithMediaType(`text/plain`) {
            val validator = new UrlValidator(List("http","https").toArray)
            val shortUrlService = new ShortUrlService
            if(validator.isValid(url) && shortUrlService.shortUrlNameIsFree(shortName)) {
              val shortUrl = shortUrlService.createShortUrl(url, shortName)
              complete(s"${shortUrl}")
            } else {
              if(validator.isValid(url))
                complete(StatusCodes.InternalServerError, "Short url name taken already!")
              else
                complete(StatusCodes.InternalServerError, "Please enter a valid url. That begins with http or https")
            }
          }
        }
      }
    }

  val redirectShortUrl =
    path(Segment) { id =>
      get {
        respondWithMediaType(`text/html`) {
          val shortUrlService = new ShortUrlService
          val shortUrl = shortUrlService.getShortUrl(id)
          if(shortUrl != null)
            redirect(shortUrl.originalUrl, StatusCodes.MovedPermanently)
          else
            complete(StatusCodes.NotFound)
        }
      }
    }

  def route = shorten ~ redirectShortUrl

}