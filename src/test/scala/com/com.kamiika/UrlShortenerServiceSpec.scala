package com.kamiika

import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest
import spray.http._
import StatusCodes._
import scala.util.Random

class UrlShortenerServiceSpec extends Specification with Specs2RouteTest with UrlShortenerService {
  def actorRefFactory = system
  
  "UrlShortenerService" should {

    "leave GET requests to other paths unhandled" in {
      Get("/kermit") ~> shorten ~> check {
        handled must beFalse
      }
    }

    "post shorten returns short shortUrl" in {
      Post("/shorten?url=http://www.google.fi") ~> shorten ~> check {
        responseAs[String].length === 6
      }
    }

    "Root is not handled" in {
      Get("/") ~> route ~> check {
        handled === false
      }
    }

    "http is handled" in {
      Post("/shorten?url=http://www.google.fi") ~> shorten ~> check {
        responseAs[String].length === 6
        val resp = response.entity.asString
        Get("/"+resp) ~> route ~> check {
          response.status should be equalTo MovedPermanently
          responseAs[String] === "This and all future requests should be directed to <a href=\"http://www.google.fi\">this URI</a>."
        }
      }
    }

    "https is handled" in {
      Post("/shorten?url=https://www.facebook.com") ~> shorten ~> check {
        responseAs[String].length === 6
        val resp = response.entity.asString
        Get("/"+resp) ~> route ~> check {
          response.status should be equalTo MovedPermanently
          responseAs[String] === "This and all future requests should be directed to <a href=\"https://www.facebook.com\">this URI</a>."
        }
      }
    }

    "invalid url should return InternalServerError" in {
      Post("/shorten?url=www.google.fi") ~> shorten ~> check {
        response.status should be equalTo InternalServerError
        responseAs[String] === "Please enter a valid url. That begins with http or https"
      }
    }

    "return 404 if no path is found with id" in {
      Get("/abcdefghjkl") ~> redirectShortUrl ~> check {
        response.status should be equalTo NotFound
      }
    }

    "post shorten with name returns short shortUrl" in {
      Post("/shorten?url=http://www.google.fi&shortName=ggl") ~> shorten ~> check {
        responseAs[String] === "ggl"
      }
    }

    "post shorten with name fails second time with same name" in {
      Post("/shorten?url=http://www.google.fi&shortName=ggl2") ~> shorten ~> check {
        responseAs[String] === "ggl2"
      }
      Post("/shorten?url=http://www.google.fi&shortName=ggl2") ~> shorten ~> check {
        responseAs[String] === "Short url name taken already!"
      }
    }

    "post shorten fails n times" in {
      Post("/shorten?url=http://www.google.fi&shortName=googel") ~> shorten ~> check {
        responseAs[String] === "googel"
      }
      val range = 4 to 15
      for(a <- 1 to range(Random.nextInt(range length))) {
        Post("/shorten?url=http://www.google.fi&shortName=googel") ~> shorten ~> check {
          responseAs[String] === "Short url name taken already!"
        }
      }
      Post("/shorten?url=http://www.google.fi&shortName=googel") ~> shorten ~> check {
        responseAs[String] === "Short url name taken already!"
      }
    }


  }
}
