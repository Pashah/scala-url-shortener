# Url Shortener

This project is built using Scala, spray io and mongodb.

## Endpoints:

POST /shorten  
Parameters:   
Name: url Description: Link to shorten. Link should start with http or https. Required parameter  
Name: shortName Description: Optional parameter to give your chosen name to your shorten url.   
Returns: Id for the shortened link. 500 error if url is not valid and if your chosen shortName is already taken

GET /{id}  
Returns: 301 redirects the user agent to a previously stored URL. 404 error if no link stored with given id

## How to run application:

1. Git-clone this repository.

2. Change directory into your clone:

        $ cd my-project

3. Launch SBT:

        $ sbt

4. Install Mongo-DB and run it at default port 27017 or change port at MongoFactory.scala

5. Compile everything and run all tests:

        > test

6. Start the application:

        > re-start

7. Shorten urls with curl or whatever tool you like best